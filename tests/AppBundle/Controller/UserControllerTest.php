<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use AppBundle\Entity\User as User;

class UserControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }

    public function testSubscribeUser()
    {
        $user = new User('bobby', 'passbob', 'mailbob@mail.fr');

        $client = static::createClient();

        $subscribe = $client->request('POST', '/api/subscribe', []);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
