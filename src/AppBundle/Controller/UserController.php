<?php
/**
 * Created by PhpStorm.
 * User: thoma
 * Date: 01/03/2017
 * Time: 16:08
 */
namespace AppBundle\Controller;

use AppBundle\Entity\User as User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class UserController extends Controller
{
    /**
     * @Route("/user/subscribe", name="subscribe")
     */
    public function subscribeUser(Request $request) {
        $username = json_decode($request->getContent(), true)['username'];
        $password = json_decode($request->getContent(), true)['password'];
        $email = json_decode($request->getContent(), true)['email'];
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array('username' => $username));

        if (!$user) {
            $user = new User($username, $password, $email);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $message="Utilisateur créé";
        } else {
            $message="L'utilisateur existe déjà";
        }

        $response = new Response();
        $response->setContent(json_encode(array(
            'success' => true,
            'message' => $message
        )));

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     *
     * @Route("/user/authenticate", name="authenticate")
     */
    public function authenticateUser(Request $request) {
        $username = json_decode($request->getContent(), true)['username'];
        $password = json_decode($request->getContent(), true)['password'];

        $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array('username' => $username));

        $success = false;

        if ($user != null) {
            if ($password == $user->getPassword()) {
                $success = true;
            }
        }

        $response = new Response();
        $response->setContent(json_encode(array(
            'success' => $success,
        )));

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     *
     * @Route("/user/deleteUser", name="deleteUser")
     */
    public function deleteUser(Request $request) {
        $username = json_decode($request->getContent(), true)['username'];
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array('username' => $username));

        if ($user != null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            $success = true;
        }else{
            $success = false;
        }

        $response = new Response();
        $response->setContent(json_encode(array(
            'success' => $success,
        )));

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     *
     * @Route("/user/modifyPassword", name="modifyPassword")
     */
    public function modifyPassword(Request $request) {
        $username = json_decode($request->getContent(), true)['username'];
        $newPassword = json_decode($request->getContent(), true)['mdp'];
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(array('username' => $username));
        $user->setPassword($newPassword);
        $em = $this->getDoctrine()->getManager();
        $em->flush();
        $success = true;

        $response = new Response();
        $response->setContent(json_encode(array(
            'success' => $success,
        )));

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}