<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{
    /**
     * @Route("/music/search", name="searchmusic")
     */
    public function searchMusic(Request $request) {
        $search = rawurlencode(json_decode($request->getContent(), true)['search']);

        $contents = file_get_contents("http://api.deezer.com/search?q=" . $search);
        $response = new Response();
        $response->setContent(json_encode($contents));

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/movie/search", name="searchmovie")
     */
    public function searchMovie(Request $request) {
        $search = rawurlencode(json_decode($request->getContent(), true)['search']);

        $contents = file_get_contents("http://www.omdbapi.com/?s=" . $search);
        $response = new Response();
        $response->setContent(json_encode($contents));

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/art/search", name="searchart")
     */
    public function searchArt(Request $request) {
        $search = rawurlencode(json_decode($request->getContent(), true)['search']);

        $contents = file_get_contents("http://www.europeana.eu/api/v2/search.json?wskey=mJywqMT8q&query=" . $search);
        $response = new Response();
        $response->setContent(json_encode($contents));

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}
